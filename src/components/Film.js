import React, { Component, useEffect } from "react";
import { Films } from "../shared/ListOfFilms";
import { useState } from "react";
import { ThemeContext } from "./ThemeContext";
import { useContext } from "react";
import { Link } from "react-router-dom";
import { ModalCase } from "./ModalCase";
import NotificationContainer from "react-notifications/lib/NotificationContainer";
function Film({ idPlayer, setIdPlayer, loading, setLoading }) {
  const [film, setFilm] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const { theme, toggle, dark } = useContext(ThemeContext);
  const [isLogin, setIsLogin] = useState(null);
  const [value, setValue] = useState(false);
  const [APIData, setAPIData] = useState([]);
  const Film =
    APIData &&
    APIData.find((f) => {
      return f.id == idPlayer;
    });
  useEffect(() => {
    fetch("https://641465ce9172235b8693acbe.mockapi.io/films", {
      method: "GET",
    })
      .then((res) => res.json())
      .then((data) => {
        setAPIData(data);
      })
      .catch((error) => console.log(error.message));
  }, [value]);

  useEffect(
    () => {
      setLoading(false);
      setIsLogin(JSON.parse(localStorage.getItem("userLogin")));
    },
    // eslint-disable-next-line
    [loading]
  );

  const deleteFilm = (id) => {
    fetch(`https://641465ce9172235b8693acbe.mockapi.io/films/${id}`, {
      method: "DELETE",
    })
      .then((res) => res.json())
      .then((data) => {
        setValue(!value);
      })
      .catch((error) => console.log(error.message));
  };
  return (
    <div className="container">
      {APIData.map((f) => (
        <>
          <div className="column" key={f.id}>
            <div className="card">
              <img src={f.image} alt="" />
              <h3>{f.title}</h3>
              <p className="year">{f.year}</p>
              <p className="nation">{f.nation}</p>
              <div className="cssDetail">
                <button
                  className="button"
                  onClick={() => {
                    setFilm(f);
                  }}
                >
                  <h1 className="detail">
                    <Link to={`/Detail/${f.id}`}>Detail</Link>
                  </h1>
                  {isLogin && (
                    <>
                      <h1
                        className="detail"
                        style={{
                          background: "darkcyan",
                          cursor: "pointer",
                        }}
                      >
                        <Link
                          to="#"
                          onClick={() => {
                            setIdPlayer(f.id);
                            setIsOpen(true);
                          }}
                        >
                          Update
                        </Link>
                      </h1>
                      <h1
                        className="detail"
                        style={{ background: "crimson", cursor: "pointer" }}
                      >
                        <Link to="#" onClick={() => deleteFilm(f.id)}>
                          Delete
                        </Link>
                      </h1>
                    </>
                  )}
                </button>
              </div>
            </div>
          </div>
        </>
      ))}
      {isOpen && (
        <ModalCase
          idPlayer={idPlayer}
          setIsOpen={setIsOpen}
          Film={Film}
          setValue={setValue}
          value={value}
        />
      )}
      <NotificationContainer />
    </div>
  );
}

export default Film;
