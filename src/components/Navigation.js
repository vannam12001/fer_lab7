import React from "react";
import { useContext } from "react";
import { ThemeContext } from "./ThemeContext";

export default function Navigation() {
  const { theme, toggle, dark } = useContext(ThemeContext);
  return (
    <div>
      <div className="cssList"></div>
      <nav
        style={{ backgroundColor: theme.backgroundColor, color: theme.color }}
      >
        <div style={{ position: "relative" }}>
          <a
            className="switch-mode"
            href="#"
            onClick={toggle}
            style={{
              backgroundColor: theme.backgroundColor,
              color: theme.color,
              outline: "none",
            }}
            data-testid="toggle-theme-btn"
          ></a>
        </div>
      </nav>
    </div>
  );
}
