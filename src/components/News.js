import React from "react";
import { Row, Col, Card, Icon, CardTitle, Tab, Tabs } from "react-materialize";

function News() {
  return (
    <Tabs className="tab-demo z-depth-1" scope="tabs-22">
      <Tab
        options={{
          duration: 300,
          onShow: null,
          responsiveThreshold: Infinity,
          swipeable: false,
        }}
        title="New 1"
      >
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum
      </Tab>
      <Tab
        active
        options={{
          duration: 300,
          onShow: null,
          responsiveThreshold: Infinity,
          swipeable: false,
        }}
        title="New 2"
      >
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
        ut aliquip ex ea commodo consequat. Duis aute irure dolor in
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur
      </Tab>
      <Tab
        options={{
          duration: 300,
          onShow: null,
          responsiveThreshold: Infinity,
          swipeable: false,
        }}
        title="New 3"
      >
        Lorem Ipsum is simply dummy text of the printing and typesetting
        industry. Lorem Ipsum has been the industry's standard dummy text ever
        since the 1500s, when an unknown printer took a galley of type and
        scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining
        essentially unchanged. It was popularised in the 1960s with the release
        of Letraset sheets containing Lorem Ipsum passages, and more recently
        with desktop publishing software like Aldus PageMaker including versions
        of Lorem Ipsum
      </Tab>
      <Tab
        options={{
          duration: 300,
          onShow: null,
          responsiveThreshold: Infinity,
          swipeable: false,
        }}
        title="New 4"
      >
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
        ut aliquip ex ea commodo consequat. Duis aute irure dolor in
        reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
        pariatur
      </Tab>
    </Tabs>
  );
}

export default News;
