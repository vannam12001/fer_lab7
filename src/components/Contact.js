import React from 'react'
import { Button, Container, Textarea, TextInput, Select, Row, Col } from 'react-materialize'


function Contact() {
  const handleSubmit =(e)=> {
    e.preventDefault()
  }
  return (
    <Container>
      <form onClick={handleSubmit} className='ContactUS'>
        <Row>
          <Col>
          <TextInput className='cssContact' id="TextInput-38" label="Your Name" />
          </Col>
        </Row>
        <Row>
          <Col>
          <TextInput className='cssContact' id="TextInput-39" label="Your Phone" />
          </Col>
        </Row>
        <Row>
          <Col>
        <TextInput className='cssContact' email id="TextInput-41" label="Email" validate />
          </Col>
        </Row>
          <Select id='Select-46' multiple={false} onChange={function noRefCheck() { }} value="">
            <option disabled value="">
            Choose your favorite nation
            </option>
            <option value="1">
              England
            </option>
            <option value="2">
              France
            </option>
            <option value="3">
              Spain
            </option>
          </Select>
        <Row>
          <Col>
          <Textarea className='cssContact' id="Textarea-28" label="Your content" />
          </Col>
        </Row>
        <Button >Submit</Button>
      </form>
    </Container>
  )
}

export default Contact  