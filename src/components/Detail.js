import React from "react";
import { useParams } from "react-router-dom";
import { Films } from "../shared/ListOfFilms";
import { Icon } from "react-materialize";
import { useState } from "react";
import { ModalCase } from "./ModalCase";

function Detail() {
  const param = useParams();
  const id = param.id;
  const Film = Films.find((f) => {
    return f.id == id;
  });
  const [isOpen, setIsOpen] = useState(false);
  const handleOpen = () => {
    setIsOpen(!isOpen);
  };
  return (
    <div className="detail-container">
      <div className="film-card">
        <div className="film-tumb">
          <img className="imgDetail" src={Film.img} alt="a film thumbnail" />
          <div className="trailer">
            <a onClick={() => setIsOpen(true)}>
              Watch trailer <Icon>play_circle_filled</Icon>
            </a>
            {isOpen && <ModalCase setIsOpen={setIsOpen} Film={Film} />}
          </div>
        </div>
        <div className="film-details">
          <h3 className="titleDetail">{Film.title}</h3>
          <p className="yearDetail">{Film.year}</p>
          <p className="nationDetail">{Film.nation}</p>
          <p className="infoDetail">{Film.info}</p>
        </div>
      </div>
    </div>
  );
}

export default Detail;
