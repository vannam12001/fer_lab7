import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import LoginGoogle from "./LoginGoogle";
function Header({ loading, setLoading }) {
  const [isLogin, setIsLogin] = useState(null);
  useEffect(
    () => {
      setLoading(false);
      setIsLogin(JSON.parse(localStorage.getItem("userLogin")));
    },
    // eslint-disable-next-line
    [loading]
  );
  return (
    <nav>
      <div class="nav-wrapper">
        <ul id="nav-mobile" class="left hide-on-med-and-down">
          <li>
            <a href="sass.html">
              <Link to={"/"}>
                <i class="material-icons">home</i>
              </Link>
            </a>
          </li>
          {isLogin && (
            <li>
              <a href="sass.html">
                <Link to={"/addFilm"}>ADD</Link>
              </a>
            </li>
          )}

          <li>
            <a href="badges.html">
              <Link to={"/About"}>About</Link>
            </a>
          </li>
          <li>
            <a href="new.html">
              <Link to={"/News"}>News</Link>
            </a>
          </li>
          <li>
            <a href="collapsible.html">
              <Link to={"/Contact"}>Contact</Link>
            </a>
          </li>
        </ul>
        {isLogin ? (
          <div
            style={{
              paddingRight: "15px",
              display: "flex",
              alignItems: "center",
            }}
          >
            <Link
              to="#"
              style={{ paddingRight: "15px" }}
              onClick={() => {
                localStorage.removeItem("userLogin");
                setLoading(true);
              }}
            >
              Logout
            </Link>
            <img
              src={isLogin.imageUrl}
              alt=""
              style={{ width: "50px", height: "50px", borderRadius: "50%" }}
            />
          </div>
        ) : (
          <div style={{ paddingRight: "15px" }}>
            <LoginGoogle setLoading={setLoading} />
          </div>
        )}
      </div>
    </nav>
  );
}

export default Header;
