import React from 'react'
import {CollapsibleItem,Collapsible} from 'react-materialize'
import {Icon} from 'react-materialize'
function About() {
  return (
    <div className='cssAbout'>
      <Collapsible accordion>
    <CollapsibleItem
      expanded={false}
      header="Better safe than sorry. That's my motto."
      icon={<Icon>filter_drama</Icon>}
      node="div"
    >
      Better safe than sorry. That's my motto.
    </CollapsibleItem>
    <CollapsibleItem
      expanded
      header="Yeah, you do seem to have a little 'shit creek' action going."
      icon={<Icon>place</Icon>}
      node="div"
    >
      Yeah, you do seem to have a little 'shit creek' action going.
    </CollapsibleItem>
    <CollapsibleItem
      expanded={false}
      header="You know, FYI, you can buy a paddle. Did you not plan for this contingency?"
      icon={<Icon>whatshot</Icon>}
      node="div"
    >
      You know, FYI, you can buy a paddle. Did you not plan for this contingency?
    </CollapsibleItem>
    </Collapsible>
    </div>
  )
}

export default About