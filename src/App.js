import logo from "./logo.svg";
import "./App.css";
import Film from "./components/Film";
import Navigation from "./components/Navigation";
import { ThemeContext } from "./components/ThemeContext";
import { useContext } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Routes, Route } from "react-router-dom";
import Detail from "./components/Detail";
import Contact from "./components/Contact";
import About from "./components/About";
import News from "./components/News";
import { useState } from "react";
import AddFilm from "./components/AddFilm";
import "react-notifications/lib/notifications.css";
function App() {
  const { theme, toggle, dark } = useContext(ThemeContext);
  const [loading, setLoading] = useState(false);
  const [idPlayer, setIdPlayer] = useState(0);
  return (
    <div
      className="App"
      style={
        !dark
          ? { backgroundcolor: theme.backgroundColor, color: theme.color }
          : { backgroundColor: theme.backgroundColor, color: theme.color }
      }
    >
      <Header loading={loading} setLoading={setLoading} />
      <Navigation />
      <Routes>
        <Route
          path="/"
          element={
            <Film
              idPlayer={idPlayer}
              setIdPlayer={setIdPlayer}
              loading={loading}
              setLoading={setLoading}
            ></Film>
          }
        />
        <Route path="/addFilm" element={<AddFilm />}></Route>
        <Route path="/Detail/:id" element={<Detail></Detail>} />
        <Route path="/About" element={<About></About>} />
        <Route path="/News" element={<News></News>} />
        <Route path="/Contact" element={<Contact></Contact>} />
      </Routes>
      <Footer />
    </div>
  );
}

export default App;
