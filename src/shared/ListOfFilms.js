export const Films = [
  {
    id: 1,
    img: "https://upload.wikimedia.org/wikipedia/vi/2/2d/Avengers_Endgame_bia_teaser.jpg",
    title: "Avengers",
    year: "2019",
    nation: "USA",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Phim theo chân Jennifer Walters, một luật sư chuyên về các vụ án liên quan đến những siêu nhân, người cũng trở thành siêu anh hùng xanh She-Hulk.",
  },
  {
    id: 2,
    img: "https://images2.thanhnien.vn/Uploaded/nhuvnq/2022_06_17/284550373-370852648356152-3746905122681766979-n-3012.jpg",
    title: "Em và Trịnh",
    year: "2020",
    nation: "Việt Nam",
    clip: "https://www.youtube.com/embed/zzik4JB9D1Q",
    info: "Cuộc gặp gỡ định mệnh giữa Trịnh Công Sơn và Michiko mở ra chuyến hành trình ngược về thanh xuân, khám phá mối tình với các nàng thơ",
  },
  {
    id: 3,
    img: "https://upload.wikimedia.org/wikipedia/vi/4/42/%C3%81p_ph%C3%ADch_phim_M%E1%BA%AFt_bi%E1%BA%BFc.jpg",
    title: "Mắt Biếc",
    year: "2018",
    nation: "Việt Nam",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },

  {
    id: 4,
    img: "https://image.tmdb.org/t/p/w342/z2yahl2uefxDCl0nogcRBstwruJ.jpg",
    title: "House of the Dragon",
    year: "2021",
    nation: "USA",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Lấy bối cảnh ở vương triều Targaryen, thế hệ được trị vì bởi cha ông Daenerys Targaryen. Khi này, ông tổ Daenerys là Aegon người đã ra lệnh cho rồng xâm lăng rồi thống nhất Bảy Vương Quốc. Nhưng cuộc chiến bỗng trở nên khốc liệt hơn khi mà Vũ điệu của bầy Rồng xảy ra, lợi dụng vào tình thế đó hai chị em Targaryen đã lên kế hoạch tranh giành ngôi báu mà Aegon để lại.",
  },

  {
    id: 5,
    img: "https://image.tmdb.org/t/p/w342/aQJ93gdI2htbZfOLX41PnkRxW3l.jpg",
    title: "The adventure of Jogi",
    year: "2020",
    nation: "Ấn Độ",
    clip: 'https://www.youtube.com/embed/eJE2Pbg8dQ4"',
    info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  },

  {
    id: 6,
    img: "https://image.tmdb.org/t/p/w342/iNf1wC8cK6e8zgNyuMUqWFT7Din.jpg",
    title: "Drifting Home",
    year: "2020",
    nation: "Nhật Bản",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Vào một mùa hè định mệnh, nhóm học sinh tiểu học mắc kẹt trên tòa nhà chung cư bỏ hoang đang trôi dạt và phải thấu hiểu bản thân để tìm đường trở về nhà.",
  },

  {
    id: 7,
    img: "https://image.tmdb.org/t/p/w342/gJDvYUJcPEDJzB3SnQokifPUtRF.jpg",
    title: "Big Mouth",
    year: "2021",
    nation: "Hàn Quốc",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Big Mouth kể về câu chuyện về một anh chàng luật sư kém tài và không may bị cuốn vào một vụ án giết người. Để sống sót và bảo vệ gia đình mình, anh bắt buộc phải đào sâu khám phá âm mưu lớn của tầng lớp thượng lưu.",
  },

  {
    id: 8,
    img: "https://image.tmdb.org/t/p/w342/4Pc0J2UESMEzUsYELvJZREjFLXg.jpg",
    title: "The Midnight Maiden War",
    year: "2021",
    nation: "USA",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "Vi vu qua bao thế giới mê ảo trong thiết bị mô phỏng vũ trụ, bình luận viên không gian đi tìm ý nghĩa của sự sống, cái chết và những điều chưa biết khác về sự sinh tồn.",
  },

  {
    id: 9,
    img: "https://image.tmdb.org/t/p/w342/uU3YoLSaVBOaMryzhuiiRWDFbIw.jpg",
    title: "Bodies Bodies Bodies",
    year: "2022",
    nation: "Japan",
    clip: "https://www.youtube.com/embed/eJE2Pbg8dQ4",
    info: "During her family move to the suburbs, a sullen 10-year-old girl wanders into a world ruled by gods, witches, and spirits, and where humans are changed into beasts.",
  },
];
